package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class PipeTask {

    private static Logger log = LogManager.getLogger(PipeTask.class);
    private PipedReader pipedReader;
    private PipedWriter pipedWriter;

    public PipeTask() {
        pipedReader = new PipedReader();
        pipedWriter = new PipedWriter();
    }

    public void doTask() {
        Thread write = new Thread(this::writeToPipe);
        Thread read = new Thread(this::readFromPipe);
        try {
            pipedReader.connect(pipedWriter);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        write.start();
        read.start();
    }

    private void writeToPipe() {
        try {
            for (int i = 1; i <= 5; i++) {
                pipedWriter.write("data written...\n");
                pipedWriter.flush();
                Thread.sleep(2000);
            }
            pipedWriter.close();
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        }
    }

    private void readFromPipe() {
        try {
            int c;
            while ((c = pipedReader.read()) != -1) {
                System.out.print((char) c);
            }
            pipedReader.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
