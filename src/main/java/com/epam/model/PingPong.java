package com.epam.model;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {

    private static Logger log = LogManager.getLogger(PingPong.class);
    private int repeatCount;

    public PingPong(int repeatCount) {
        this.repeatCount = repeatCount;
    }

    public void doPingPong() {
        Thread ping = new Thread(printPingPong("Ping"));
        Thread pong  = new Thread(printPingPong("Pong"));
        ping.start();
        pong.start();
    }

    private Runnable printPingPong(String pingOrPong) {
        return () -> {
            boolean flag = true;
            int count = 1;
            while (flag) {
                synchronized (this) {
                    log.info(pingOrPong);
                    count++;
                    notify();
                    try {
                        if (count <= repeatCount){
                            wait();
                        }else{
                            flag = false;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }
}
